clear
echo "The script exe_mesh_Re starts now"
echo "Hello, $USER!"

cd baseFolder
sn=1
r=1
echo "Hello1"
#loop on the Reynolds number 
for Re in 45 60 80 100 120 140
do
m=1
for Gridx in 128 256 512 1024
do
mkdir ../Name${r}_${m}
Gridy=$((${Gridx} * 4))
sed -e "s/rrr/${Re}/" -e "s/xxxx/${Gridx}/" -e "s/yyyy/${Gridy}/" input.bkp > input.data
sed -e "s/yy/${sn}/" JobScript.bkp > JobScript
cp input.data ../Name${r}_${m}
cp JobScript ../Name${r}_${m}
echo "add a message that describes what is happening"
sn=$(( $sn + 1 ))
m=$(( $m + 1 ))
done
r=$(( $r + 1 ))
done
cd ../
